import { useState } from 'react';
import { Button, Space, Table as Tbl } from 'antd';
import BookModal from './BookModal';

type Props = {
    isAdmin?: boolean;
    columns: object[];
    data: object[];
};

const Table = (props: Props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const add = () => {
        setIsModalVisible(true);
    };

    const setKey = (record: any) => {
        return record._id;
    };

    return (
        <>
            {!props.isAdmin && (
                <Space style={{ marginBottom: 16 }}>
                    <Button onClick={add}>Add book</Button>
                </Space>
            )}
            <Tbl
                columns={props.columns}
                dataSource={props.data}
                rowKey={setKey}
            />

            {isModalVisible && (
                <BookModal
                    isModalVisible={isModalVisible}
                    setIsModalVisible={setIsModalVisible}
                />
            )}
        </>
    );
};

export default Table;
