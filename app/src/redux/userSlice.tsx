import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './store';
import fetchData, { getUsersParams, loginParams, registerParams } from './api';

export type User = {
    _id: string;
    firstName: string;
    lastName: string;
    username: string;
    isAdmin?: boolean;
};

export interface UserState {
    user: User;
    users: User[];
}

const initialState: UserState = {
    user: {
        _id: '',
        firstName: '',
        lastName: '',
        username: '',
    },
    users: [],
};

export const register = createAsyncThunk(
    'user/getUser',
    async (body: object) => {
        console.log('regsiter thunl');
        return fetchData(registerParams(body));
    }
);

export const login = createAsyncThunk('user/getUser', async (body: object) => {
    return fetchData(loginParams(body));
});

export const getUsers = createAsyncThunk('user/getUsers', async () => {
    return fetchData(getUsersParams());
});

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        saveUser: (state, action: PayloadAction<any>) => {
            state.user = action.payload;
        },
        resetUser: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(register.fulfilled, (state, action) => {
                localStorage.setItem('token', action.payload.token);
                state.user = action.payload.user;
            })
            .addCase(register.rejected, (state) => {
                console.error('register.rejected');
            })
            .addCase(getUsers.fulfilled, (state, action) => {
                state.users = action.payload;
            })
            .addCase(getUsers.rejected, (state) => {
                console.error('getUsers.rejected');
            });
    },
});

export const { saveUser, resetUser } = userSlice.actions;

export const selectUser = (state: RootState) => state.user.user;
export const selectUsers = (state: RootState) => state.user.users;

export default userSlice.reducer;
