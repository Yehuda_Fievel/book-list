import { Tabs } from 'antd';
import LoginRegister from '../components/LoginRegister';

const { TabPane } = Tabs;

const Home = () => (
    <Tabs defaultActiveKey="1" centered>
        <TabPane tab="Login" key="1">
            <LoginRegister type="login" />
        </TabPane>
        <TabPane tab="Register" key="2">
            <LoginRegister type="register" isRegister />
        </TabPane>
    </Tabs>
);

export default Home;
