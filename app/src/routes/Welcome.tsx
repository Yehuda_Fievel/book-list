import { Button } from 'antd';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Table from '../components/Table';
import { getBooksApi, resetBook, selectBooks } from '../redux/bookSlice';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import {
    getUsers,
    resetUser,
    selectUser,
    selectUsers,
} from '../redux/userSlice';

const Welcome = () => {
    const user = useAppSelector(selectUser);
    const users = useAppSelector(selectUsers);
    const books = useAppSelector(selectBooks);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(getBooksApi());
        if (user.isAdmin) {
            dispatch(getUsers());
        }
    }, [dispatch, user]);

    const booksColumns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Author',
            dataIndex: 'author',
            key: 'author',
        },
    ];

    const usersColumns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'First name',
            dataIndex: 'firstName',
            key: 'firstName',
        },
        {
            title: 'Last name',
            dataIndex: 'lastName',
            key: 'lastName',
        },
    ];

    const logout = () => {
        localStorage.removeItem('token');
        dispatch(resetUser());
        dispatch(resetBook());
        navigate('/', { replace: true });
    };

    return (
        <>
            <Button onClick={logout} type="primary">
                Logout
            </Button>
            {user.isAdmin && (
                <Table columns={usersColumns} data={users} isAdmin />
            )}
            <Table columns={booksColumns} data={books} />
        </>
    );
};

export default Welcome;
