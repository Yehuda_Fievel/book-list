const express = require("express");
const router = express.Router();
const middleware = require('../middleware');
const usersCtrl = require("./usersCtrl");

router.all('*', middleware.authenticate, middleware.checkIsAdmin);

router.get("/", usersCtrl.get);

module.exports = router;
