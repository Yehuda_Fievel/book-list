const bcrypt = require('bcrypt');
const usersModels = require('../../models/users');
const jwt = require('../../utils/jwt');

const saltRounds = 10;

const register = async (req, res, next) => {
    try {
        req.body.password = await bcrypt.hash(req.body.password, saltRounds);
        const result = await usersModels.insertOne(req.body);
        const user = { ...req.body, _id: result.insertedId };
        const token = jwt.sign(user._id, req.body.isAdmin);
        res.status(201).json({ user, token });
    } catch (error) {
        next(error);
    }
};

const login = async (req, res, next) => {
    try {
        const user = await usersModels.findOne({ username: req.body.username });
        const isHashed = await bcrypt.compare(req.body.password, user.password);
        if (!isHashed) {
            return res.sendStatus(401);
        }
        const token = jwt.sign(user._id, user.isAdmin);
        res.status(200).json({ user, token });
    } catch (error) {
        next(error);
    }
};

module.exports = {
    register,
    login,
};
