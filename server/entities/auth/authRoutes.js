const express = require('express');
const router = express.Router();
const authValidation = require('./authValidation');
const authCtrl = require('./authCtrl');

router.post('/register', authValidation.register, authCtrl.register);

router.post('/login', authValidation.login, authCtrl.login);

module.exports = router;
