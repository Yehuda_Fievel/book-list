const Joi = require('joi');
const validate = require('../validation');

const create = (req, res, next) => {
    const schema = Joi.object().keys({
        title: Joi.string().required(),
        author: Joi.alternatives().try(Joi.array(), Joi.string()).required(),
    });
    validate({ schema, body: req.body, next });
};

module.exports = {
    create,
};
