module.exports = {
    MONG_URI: process.env.MONG_URI,
    DB_NAME: process.env.DB_NAME,
    JWT_SECRET: process.env.JWT_SECRET,
};
