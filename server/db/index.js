const init = require('./init');
const Models = require('./Models');

module.exports = { Models, init };
