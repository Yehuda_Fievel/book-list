const db = require('./init');

module.exports = class Models {
    /**
     *
     * @constructor
     * @param {string} collection
     */
    constructor(collection) {
        this.collection = collection;
    }

    insertOne = (body) => {
        return db.db.collection(this.collection).insertOne(body);
    };

    findOne = (query) => {
        return db.db.collection(this.collection).findOne(query);
    };

    find = (query = {}) => {
        return db.db.collection(this.collection).find(query).toArray();
    };
};
