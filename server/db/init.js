const { MongoClient } = require('mongodb');
const url = 'mongodb+srv://yehuda:1234@cluster0.12kya.mongodb.net/?retryWrites=true&w=majority';
const dbName = 'dbName';

class Init {
    constructor() {
        const client = new MongoClient(url);
        this.connect(client);
    }

    connect = async (client) => {
        await client.connect();
        console.log('Connected to MongoDB')
        this.db = client.db(dbName);
    };
}

module.exports = new Init();
