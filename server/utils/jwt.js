const jwt = require('jsonwebtoken');
const config = require('../config');

const sign = (_id, isAdmin) => {
    return jwt.sign({ _id, isAdmin: isAdmin ? true : false }, config.JWT_SECRET);
};

const verify = (token) => {
    return jwt.verify(token, config.JWT_SECRET);
};

module.exports = {
    sign,
    verify,
};
